import { Fragment, useContext } from 'react';
import { Navbar, Nav, Container, Form, FormControl, Button} from 'react-bootstrap'
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

// AppNavbar component
export default function AppNavbar(){
	// State to store the user information stored in the login page
	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user);

	const { user } = useContext(UserContext);

	return(
		<Navbar  bg="dark" variant="dark" expand="lg" sticky="top">
		  <Container fluid>
		    <Navbar.Brand href="#">Navbar scroll</Navbar.Brand>
		    <Navbar.Toggle aria-controls="navbarScroll" />
		    <Navbar.Collapse id="navbarScroll">
		      <Nav
		        className="me-auto my-2 my-lg-0"
		        style={{ maxHeight: '100px' }}
		        navbarScroll
		      >
		        <Nav.Link as={NavLink} to="/" exact>Home</Nav.Link>
		        <Nav.Link as={NavLink} to="/products" exact>Products</Nav.Link>
		      <Form className="d-flex">
		        <FormControl
		          type="search"
		          placeholder="Search"
		          className="me-2"
		          aria-label="Search"
		        />
		        <Button variant="outline-success">Search</Button>

		      <Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
		      <Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
		      <Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>

		      </Form>
		      </Nav>

		    </Navbar.Collapse>
		  </Container>
		</Navbar>

	)
}