import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
// import CourseCard from '../components/CourseCard'

export default function Home(){
	const data={
		title: "Cool Shop",
		content: "Good quality products available for order",
		destination: "/products",
		label: "Shop now"
	}

	return(
		<Fragment>
			<Banner data={data}/>
		</Fragment>
	)
}